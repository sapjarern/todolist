from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name="Index"),
    url(r'^addtask/$', views.addtask, name="Addtask"),
    url(r'^dotask/$', views.dotask, name="Dotask"),
]