from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from .models import todotask


# Create your views here.
def index(request):
    dotask = todotask.objects.filter(dostatus=True)
    undotask = todotask.objects.filter(dostatus=False)
    context = {
        'dotask': dotask,
        'undotask': undotask,
    }
    return render(request, 'todolists/index.html', context)


def addtask(request):
    taskname = request.POST['addtask']
    # taskname = "Test 2"
    task = todotask(taskname=taskname, dostatus=False)
    task.save()
    return HttpResponseRedirect(reverse('todolists:Index'))

def dotask(request):
    if request.method == 'GET':
        taskid = request.GET['taskid']
        try:
            task = todotask.objects.get(id = taskid)
        except todotask.DoesNotExist:
            return HttpResponseRedirect(reverse('todolists:Index'))
        task.dostatus = True
        task.save()
        return HttpResponse("Success")
    else:
        return HttpResponse("Fail")
