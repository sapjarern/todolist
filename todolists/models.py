from django.db import models


# Create your models here.
class todotask(models.Model):
    taskname = models.CharField(max_length=200)
    dostatus = models.BooleanField(default=False)

    def __str__(self):
        return self.taskname
